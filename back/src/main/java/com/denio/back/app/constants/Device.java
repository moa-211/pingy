package com.denio.back.app.constants;

public enum Device {
    ALREADY_EXISTS("Устройство с таким именем/адресом уже существует"),
    ADD_SUCCESS("Устройство успешно добавлено"),
    NOT_EXISTS("Такого устройства не существует"),
    REMOVE_SUCCESS("Устройство успешно удалено");
    private final String message;

    Device(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
