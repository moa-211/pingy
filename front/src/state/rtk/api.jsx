import { useSelector, useDispatch, Provider as ReduxProvider } from 'react-redux';
import store from './store.js';
import { setAdding, setIp, setName, selectDevice, clearHighlighted, setPort, setPinger,
         getDevices, addDevice, pingDevice, pingTimeDevice, checkPortDevice, removeDevice, 
         setRefreshFlag} from './slices/main.js';
import { setPassword, setToken, setUsername,
         register, login } from './slices/auth.js';
import { setMessage } from './slices/info.js';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor } from './store.js';


function buildProvider() {
    return (props) => {
        return (
            <ReduxProvider store = {store}>
                <PersistGate loading={null} persistor={persistor}>
                    {props.children}
                </PersistGate>
            </ReduxProvider>
        )
    }   
}


function useUsernameListener() {
    return useSelector((state) => state.auth.username);
}

function useUsernameDispatcher() {
    const dispatch = useDispatch();
    return (username) => dispatch(setUsername(username));
}


function usePasswordListener() {
    return useSelector((state) => state.auth.password);
}

function usePasswordDispatcher() {
    const dispatch = useDispatch();
    return (password) => dispatch(setPassword(password));
}


function useTokenListener() {
    return useSelector((state) => state.auth.token);
}

function useTokenDispatcher() {
    const dispatch = useDispatch();
    return (token) => dispatch(setToken(token));
}


function useMessageListener() {
    return useSelector((state) => state.info.message);
}

function useMessageDispatcher() {
    const dispatch = useDispatch();
    return (message) => dispatch(setMessage(message));
}


function useDevicesListener() {
    return useSelector((state) => state.main.devices);
}

function useDevicesDispatcher() {
    const dispatch = useDispatch();
    return {
        get: () => dispatch(getDevices()),
        ping: (id) => dispatch(setPinger(id))
    };
}


function useAddingListener() {
    return useSelector((state) => state.main.adding);
}

function useAddingDispatcher() {
    const dispatch = useDispatch();
    return (value) => dispatch(setAdding(value));
}


function useNameListener() {
    return useSelector((state) => state.main.name);
}

function useNameDispatcher() {
    const dispatch = useDispatch();
    return (name) => dispatch(setName(name));
}


function useIpListener() {
    return useSelector((state) => state.main.ip);
}

function useIpDispatcher() {
    const dispatch = useDispatch();
    return (ip) => dispatch(setIp(ip));
}


function usePortListener() {
    return useSelector((state) => state.main.port);
}

function usePortDispatcher() { 
    const dispatch = useDispatch();
    return (port) => dispatch(setPort(port));
}


function usePingTimeListener() {
    return useSelector((state) => state.main.pingTime);
}


function useDeviceListener(id) {
    return useSelector((state) => state.main.devices.find(device => device.id === id));
}

function useDeviceDispatcher() {
    const dispatch = useDispatch();
    return {
        active: (id) => dispatch(pingDevice(id)),
        add: () => dispatch(addDevice()),
        select: (id) => dispatch(selectDevice(id)),
        pingTime: () => dispatch(pingTimeDevice()),
        checkPort: () => dispatch(checkPortDevice()),
        remove: (id) => dispatch(removeDevice(id))
    };
}


function useHighlightedListener() {
    return useSelector(state => state.main.highlighted);
}

function useHighlightedDispatcher() {
    const dispatch = useDispatch();
    return {
        clear: () => dispatch(clearHighlighted())
    };
}


function useRegisterDispatcher() {
    const dispatch = useDispatch();
    return () => dispatch(register());
}

function useLoginDispatcher() {
    const dispatch = useDispatch();
    return () => dispatch(login());
}

function useRefreshFlagListener() {
    return useSelector((state) => state.main.refresh);
}

function useRefreshFlagDispatcher() {
    const dispatch = useDispatch();
    return () => dispatch(setRefreshFlag(false));
}


export {buildProvider, 
    
    useUsernameListener, useUsernameDispatcher,
    usePasswordListener, usePasswordDispatcher,
    useMessageListener, useMessageDispatcher,
    useTokenListener, useTokenDispatcher,
    useDevicesListener, useDevicesDispatcher,
    useAddingListener, useAddingDispatcher,
    useNameListener, useNameDispatcher,
    useIpListener, useIpDispatcher,
    useDeviceListener, useDeviceDispatcher,
    useHighlightedListener, useHighlightedDispatcher,
    usePortListener, usePortDispatcher,
    usePingTimeListener,
    useRefreshFlagListener, useRefreshFlagDispatcher,
    
    useRegisterDispatcher, useLoginDispatcher};