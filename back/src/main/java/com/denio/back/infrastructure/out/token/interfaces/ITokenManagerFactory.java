package com.denio.back.infrastructure.out.token.interfaces;

import com.denio.back.app.interfaces.ITokenManager;

public interface ITokenManagerFactory {
    public ITokenManager createInstance();
}