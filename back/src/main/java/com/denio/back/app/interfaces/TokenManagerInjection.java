package com.denio.back.app.interfaces;

public interface TokenManagerInjection {
    public void injectTokenManager(ITokenManager manager);
}
