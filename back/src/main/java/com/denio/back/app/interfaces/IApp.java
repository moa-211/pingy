package com.denio.back.app.interfaces;

import com.denio.back.app.constants.Device;
import com.denio.back.app.constants.Ping;
import com.denio.back.app.constants.Register;
import com.denio.back.app.constants.Port;
import com.denio.back.app.dto.DeviceDTO;

import java.util.List;
import java.util.Map;

public interface IApp {
    public String login(String username, String password);
    public Register register(String username, String password);

    public Ping ping(String ipAddress);
    public long ping(String ipAddress, boolean timeSpent);
    public void pingDevicesSendToClient();
    public Port checkPort(String ipAddress, int portNumber);

    public List<DeviceDTO> getDevices(String username);
    public int addDevice(String ipAddress, String name, String username);
    public Device removeDevice(int id);

    public Map<String, String> validateToken(String token);
}
