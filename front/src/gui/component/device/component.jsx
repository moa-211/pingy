import { useDeviceDispatcher, useDeviceListener } from '../../../state/rtk/api';
import styles from './style.module.css';

function Device( { id } ) {
    const device = useDeviceListener(id);
    const dispatcher = useDeviceDispatcher();

    let className = styles.device;
    if (device.active) className += ` ${styles.active}`;
    if (device.highlight) className += ` ${styles.highlight}`;

    const handleClick = event => {
        dispatcher.select(id);
        event.stopPropagation();
    };

    return (
        <div className={className} onClick={handleClick}>
            <span>{device.name}</span>
        </div>
    );
}

export default Device;