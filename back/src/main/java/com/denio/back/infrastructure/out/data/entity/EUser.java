package com.denio.back.infrastructure.out.data.entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "\"USERS\"")
public class EUser implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    public Integer getID() {
        return ID;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setID(Integer userID) {
        ID = userID;
    }

    public void setUsername(String userName) {
        username = userName;
    }

    public void setPassword(String userPassword) {
        password = userPassword;
    }
}

