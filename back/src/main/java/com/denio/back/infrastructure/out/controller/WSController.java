package com.denio.back.infrastructure.out.controller;

import com.denio.back.app.interfaces.IClientTransporter;
import com.denio.back.utils.uuid.interfaces.IUUIDGenerator;
import jakarta.enterprise.inject.Default;
import jakarta.inject.Inject;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnMessage;
import jakarta.websocket.Session;
import jakarta.websocket.server.ServerEndpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint("/monitoring")
public class WSController implements IClientTransporter {
    @Inject
    @Default
    IUUIDGenerator idGenerator;

    private final static ConcurrentHashMap<String, Session> idSession = new ConcurrentHashMap<>();
    private final static ConcurrentHashMap<Session, String> sessionID = new ConcurrentHashMap<>();
    private final static ConcurrentHashMap<String, List<String>> usernameIDs = new ConcurrentHashMap<>();
    @Override
    public void sendToUser(String username, String message) {
        try {
            for (String ID : usernameIDs.get(username)) {
                Session session = idSession.get(ID);

                if (!session.isOpen()) {
                    removeClient(session);
                }

                session.getBasicRemote().sendText(message);
            }
        }
        catch (Exception e) {
            System.out.println("EXCEPTION IN WSCONTROLLER");
        }
    }

    @Override
    public List<String> getActiveClientNames() {
        return new ArrayList<>(usernameIDs.keySet());
    }

    @OnClose
    public void connectionClosed(Session session) {
        removeClient(session);
    }

    @OnMessage
    public void registerClient(Session session, String username) {
        addClient(session, username);
    }

    public void removeClient(Session session) {
        String ID = sessionID.remove(session);
        idSession.remove(ID);
        for (String username : usernameIDs.keySet()) {
            List<String> IDs = usernameIDs.get(username);
            IDs.remove(ID);
            if (IDs.isEmpty()) usernameIDs.remove(username);
        }
    }

    public void addClient(Session session, String username) {
        String ID = idGenerator.getUUID();
        idSession.put(ID, session);
        sessionID.put(session, ID);

        if (!usernameIDs.containsKey(username)) {
            List<String> IDs = new ArrayList<>();
            IDs.add(ID);
            usernameIDs.put(username, IDs);
        }
        else usernameIDs.get(username).add(ID);
    }
}