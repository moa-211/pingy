package com.denio.back.infrastructure.in.controller;

import com.denio.back.app.constants.Register;
import com.denio.back.app.dto.UserDTO;
import com.denio.back.app.interfaces.IApp;
import com.denio.back.infrastructure.builder.interfaces.Built;
import com.denio.back.infrastructure.in.controller.interfaces.IController;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Path("/auth")
public class AuthController implements IController {
    Jsonb jsonb = JsonbBuilder.create();
    @Inject
    @Built
    private IApp app;

    @GET
    @Produces("application/json")
    public Response login(
            @DefaultValue("")
            @QueryParam("username")
            String username,
            @DefaultValue("")
            @QueryParam("password")
            String password) {
        if (username.matches("|undefined|null") || password.matches("|undefined|null"))
            return sendResponse("Логин и/или пароль не указаны в качестве параметров", Response.Status.BAD_REQUEST);

        String token = app.login(username, password);
        return sendResponse(token);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response register(String userInfo) {
        UserDTO body;
        try {
            body = jsonb.fromJson(userInfo, UserDTO.class);
        }
        catch (Exception e) {
            return sendResponse("Некорректный запрос", Response.Status.BAD_REQUEST);
        }

        String username = body.getUsername();
        String password = body.getPassword();

        if (username == null || password == null || username.matches("|undefined|null") || password.matches("|undefined|null"))
            return sendResponse("Некорректный запрос", Response.Status.BAD_REQUEST);

        Register result = app.register(body.getUsername(), body.getPassword());

        return sendResponse(result.getMessage());
    }
}