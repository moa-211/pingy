package com.denio.back.app.interfaces;

import java.util.List;

public interface IClientTransporter {
    public void sendToUser(String username, String message);
    public List<String> getActiveClientNames();
}
