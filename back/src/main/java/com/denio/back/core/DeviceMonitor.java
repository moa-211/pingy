package com.denio.back.core;

import com.denio.back.core.interfaces.IDeviceMonitor;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class DeviceMonitor implements IDeviceMonitor {
    @Override
    public int ping(String ipAddress) {
        try {
            InetAddress inet = InetAddress.getByName(ipAddress);

            if (!inet.isReachable(1000)) return -1; // UNREACHABLE

            return 0; // SUCCESS
        }
        catch (UnknownHostException e) {
            System.out.println("ping Этот хост неизвестен");
            return -2; // BAD_IP
        }
        catch (IOException e) {
            System.out.println("ping IOException");
            return -3; // UNKNOWN
        }
    }

    @Override
    public long ping(String ipAddress, boolean timeSpent) {
        try {
            if (timeSpent) {
                long currentTime = System.currentTimeMillis();
                boolean isPinged = InetAddress.getByName(ipAddress).isReachable(1000);
                currentTime = System.currentTimeMillis() - currentTime;
                return isPinged ? currentTime : -1; // UNREACHABLE
            }
            else return 0; // SUCCESS WITHOUT TIME
        }
        catch (UnknownHostException e) {
            System.out.println("pingWithTime Этот хост неизвестен");
            return -2; // BAD_IP
        }
        catch (IOException e) {
            System.out.println("pingWithTime IOException");
            return -3; // UNKNOWN
        }
    }

    @Override
    public int checkPort(String ipAddress, int portNumber) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ipAddress, portNumber), 1000);
            socket.close();
            return 0; // SUCCESS
        }
        catch (UnknownHostException e) {
            System.out.println("checkPort Этот хост неизвестен");
            return -2; // BAD_IP
        }
        catch (IOException e) {
            System.out.println("checkPort IOException");
            return -1; // UNREACHABLE
        }
        catch (IllegalArgumentException e) {
            System.out.println("checkPort IllegalArgumentException");
            return -3; // BAD_PORT
        }
    }
}
