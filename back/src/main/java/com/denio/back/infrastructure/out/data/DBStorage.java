package com.denio.back.infrastructure.out.data;

import com.denio.back.app.dto.DeviceDTO;
import com.denio.back.app.interfaces.IStorage;
import com.denio.back.infrastructure.out.data.entity.EDevice;
import com.denio.back.infrastructure.out.data.entity.EUser;
import com.denio.back.utils.security.factories.HasherFactory;
import com.denio.back.utils.security.interfaces.IHasher;
import jakarta.annotation.Resource;
import jakarta.enterprise.inject.Default;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import jakarta.transaction.UserTransaction;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

public class DBStorage implements IStorage {
    private EntityManager entityManager;

    @Resource
    private UserTransaction userTransaction;

    private IHasher hasher;

    public DBStorage() {
        try {
            InitialContext context = new InitialContext();
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("back_pu");
            entityManager = emf.createEntityManager();
            userTransaction = (UserTransaction) context.lookup("java:comp/UserTransaction");
            hasher = new HasherFactory().createInstance();
        }
        catch (NamingException e) {
            e.printStackTrace();
            System.out.println("NamingException in catch DBExplorer");
        }
    }

    @Override
    public int findUser(String username, String password) {
        int id = -1;

        try {
            String jpql = "SELECT u FROM EUser u WHERE u.username = :name AND u.password = :password";
            Query query = entityManager.createQuery(jpql);
            query.setParameter("name", username);
            query.setParameter("password", hasher.encode(password));
            List<EUser> users = query.getResultList();

            if (users != null && !users.isEmpty()) {
                id = users.get(0).getID();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in findUser");
        }

        return id;
    }

    @Override
    @Transactional
    public boolean addUser(String username, String password) {
        try {
            userTransaction.begin();
            entityManager.joinTransaction();

            String jpql = "SELECT u from EUser u WHERE u.username = :name";
            Query query = entityManager.createQuery(jpql);
            query.setParameter("name", username);
            List<EUser> users = query.getResultList();
            if (users != null && !users.isEmpty()) return false;

            EUser user = new EUser();
            user.setUsername(username);
            user.setPassword(hasher.encode(password));

            entityManager.persist(user);
            userTransaction.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in DBStorage addUser");
            return false;
        }

        return true;
    }

    @Override
    public List<DeviceDTO> getDevices(String username) {
        List<DeviceDTO> devices = new ArrayList<>();

        String jpql = "SELECT d FROM EDevice d WHERE d.user_id = (SELECT u.ID FROM EUser u WHERE u.username = :name)";
        Query query = entityManager.createQuery(jpql);
        query.setParameter("name", username);
        List<EDevice> entities = query.getResultList();

        if (entities != null && !entities.isEmpty()) {
            for (EDevice d : entities) {
                DeviceDTO device = new DeviceDTO();
                device.setId(d.getID());
                device.setIp(d.getIp());
                device.setName(d.getName());
                devices.add(device);
            }
        }

        return devices;
    }

    @Override
    public int addDevice(String ip, String name, String username) {
        try {
            String jpql1 = "SELECT u from EUser u WHERE u.username = :name";
            Query query1 = entityManager.createQuery(jpql1);
            query1.setParameter("name", username);
            List<EUser> users = query1.getResultList();
            int user_id = users.get(0).getID();

            String jpql2 = "SELECT d FROM EDevice d WHERE (d.ip = :ip OR d.name = :name) AND d.user_id = :user_id";
            Query query2 = entityManager.createQuery(jpql2);
            query2.setParameter("ip", ip);
            query2.setParameter("name", name);
            query2.setParameter("user_id", user_id);
            List<EDevice> devices = query2.getResultList();
            if (devices != null && !devices.isEmpty()) return -1;

            userTransaction.begin();
            entityManager.joinTransaction();

            EDevice device = new EDevice();
            device.setIp(ip);
            device.setName(name);
            device.setUser_id(user_id);

            entityManager.persist(device);
            userTransaction.commit();

            Query query3 = entityManager.createQuery(jpql2);
            query3.setParameter("ip", ip);
            query3.setParameter("name", name);
            query3.setParameter("user_id", user_id);
            List<EDevice> devicesId = query3.getResultList();
            return devicesId.get(0).getID();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in DBStorage addUser");
            return -1;
        }
    }

    @Override
    public String getDeviceOwner(int deviceID) {
        try {
            String jpql = "SELECT u from EUser u WHERE u.ID = (SELECT d.user_id from EDevice d WHERE d.ID = :deviceID)";
            Query query = entityManager.createQuery(jpql);
            query.setParameter("deviceID", deviceID);
            List<EUser> users = query.getResultList();

            if (users != null && !users.isEmpty()) {
                return users.get(0).getUsername();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in DBStorage addUser");
        }

        return "";
    }

    @Override
    public boolean removeDevice(int id) {
        try {
            userTransaction.begin();
            entityManager.joinTransaction();

            EDevice device = entityManager.find(EDevice.class, id);
            if (device == null) return false;
            entityManager.remove(device);

            userTransaction.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in modifyTask");
        }

        return true;
    }
}
