package com.denio.back.infrastructure.out.token;

import com.denio.back.app.interfaces.ITokenManager;
import io.jsonwebtoken.Jwts;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.HashMap;
import java.util.Map;

public class TokenManager implements ITokenManager {
    private static final byte[] secretBytes = "TESTTESTTESTTESTTESTTESTETTSTSTETSETET".getBytes();
    private static final SecretKey secretKey = new SecretKeySpec(secretBytes, 0, secretBytes.length, "HmacSHA256");

    @Override
    public String generateToken(String username, String passwd) {
        return Jwts.builder().subject(username).claim("password", passwd).signWith(secretKey).compact();
    }

    @Override
    public Map<String, String> getTokenInfo(String token) {
        Map<String, String> map = new HashMap<>();

        try {
            String username = (String) Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).getPayload().get("sub");
            String password = (String) Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).getPayload().get("password");
            map.put("username", username);
            map.put("password", password);
        }
        catch (Exception e) {
            System.out.println("Exception in getTokenInfo");
        }

        return map;
    }
}
