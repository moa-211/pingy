import MessageWindow from '../message-window/component';
import { useAddingListener, useAddingDispatcher,
    useNameListener, useNameDispatcher, 
    useIpListener, useIpDispatcher, 
    useMessageDispatcher,
    useDeviceDispatcher} from '../../../state/rtk/api';
import { Dialog } from '@headlessui/react';
import styles from './style.module.css';

function AddWindow() {
    const addingFlag = useAddingListener();
    const setAdding = useAddingDispatcher();
    const name = useNameListener();
    const setName = useNameDispatcher();
    const ip = useIpListener();
    const setIp = useIpDispatcher();
    const setMessage = useMessageDispatcher();

    const deviceDispatcher = useDeviceDispatcher();

    const handleHide = () => setAdding(false);
    const handleName = event => setName(event.target.value);
    const handleIp = event => setIp(event.target.value);

    const handleClick = (event) => {
        if (!name) {
            setMessage('Введите имя устройства!');
            return;
        }
        if (!ip) {
            setMessage('Введите адрес устройства!');
            return;
        }

        deviceDispatcher.add();
        event.stopPropagation();
    };

    return (
        <Dialog open={addingFlag} onClose={handleHide}>
            <div className={addingFlag ? styles.modal + ' ' + styles.modalActive : styles.modal }>
                <Dialog.Panel className={addingFlag ? styles.modalContent + ' ' + styles.modalContentActive : styles.modalContent}>
                    <input value={name} type="text" placeholder="Имя устройства" onChange={handleName} />
                    <input value={ip} type="text" placeholder="Адрес устройства" onChange={handleIp} />
                    <div className={ styles.btnDiv }>
                        <button onClick={handleClick}>Добавить устройство</button>
                    </div>
                    <MessageWindow />
                </Dialog.Panel>
            </div>
        </Dialog>
    );
}

export default AddWindow;