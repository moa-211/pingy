import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import QuerierInjection from './injection';
import WSTransporter from "../../../transport/in/ws";


const querier = new QuerierInjection().querier;


const getDevices = createAsyncThunk('main/getDevices', async (arg, { getState }) => {
    const state = getState();
    const result = await querier.getDevices(state.auth.token);
    return result.map(device => ({ip: device.ip, name: device.name, id: device.id}));
});

const addDevice = createAsyncThunk('main/addDevice', async (arg, { getState }) => {
    const state = getState();
    const device = { ip: state.main.ip, name: state.main.name };
    const result = await querier.addDevice(device, state.auth.token);
    return {device, id: Number(result.message)};
});

const pingDevice = createAsyncThunk('main/pingDevice', async (id, { getState }) => {
    const state = getState();
    const device = state.main.devices.find(device => device.id === id);
    const result = await querier.ping(device.ip, state.auth.token);
    return {id, active: result.message === 'Хост доступен'};
});

const removeDevice = createAsyncThunk('main/removeDevice', async (id, { getState }) => {
    const state = getState();
    const result = await querier.removeDevice(id, state.auth.token);
    return {result: result.message, id};
});

const pingTimeDevice = createAsyncThunk('main/pingTimeDevice', async (arg, { getState }) => {
    const state = getState();
    const result = await querier.pingTime(state.main.highlighted.ip, state.auth.token);
    return result.message;
});

const checkPortDevice = createAsyncThunk('main/checkPortDevice', async (arg, { getState }) => {
    const state = getState();
    const result = await querier.checkPort(state.main.highlighted.ip, state.main.port, state.auth.token);
    return result.message;
});

const mainSlice = createSlice({
    name: 'main',
    initialState: {
        adding: false,
        name: '',
        ip: '',
        devices: [],
        highlighted: {},
        pingTime: 0,
        port: 0,
        refresh: false
    },
    reducers: {
        setAdding: (state, action) => {
            state.adding = action.payload;
        },
        setName: (state, action) => {
            state.name = action.payload;
        },
        setIp: (state, action) => {
            state.ip = action.payload;
        },
        selectDevice: (state, action) => {
            state.devices.forEach(device => {
                device.highlight = (device.id === action.payload);
                if (device.id === action.payload) state.highlighted = device;
            });
            state.pingTime = 0;
        },
        setPort: (state, action) => {
            state.port = action.payload;
        },
        clearHighlighted: (state, action) => {
            state.highlighted = {};
        },
        setPinger: (state, action) => {
            new WSTransporter(action.payload);
        },
        setActive: (state, action) => {
            const device = state.devices.find(device => device.id === action.payload.id);
            if (device) device.active = action.payload.status;
            if (device.id === state.highlighted.id) state.highlighted = device;
        },
        setRefreshFlag: (state, action) => {
            state.refresh = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getDevices.fulfilled, (state, action) => {
            state.devices = action.payload;
            state.highlighted = {};
            state.port = 0;
            state.pingTime = 0;
        });
        builder.addCase(addDevice.fulfilled, (state, action) => {
            if (action.payload.id !== -1) {
                const device = action.payload.device;
                device.id = action.payload.id;
                state.devices.push(device);
            }
        });
        builder.addCase(pingDevice.fulfilled, (state, action) => {
            const device = state.devices.find(device => device.id === action.payload.id);
            if (device) device.active = action.payload.active;
            if (device.id === state.highlighted.id) state.highlighted = device;
        });
        builder.addCase(removeDevice.fulfilled, (state, action) => {
            if (action.payload.result === 'Устройство успешно удалено') {
                const index = state.devices.findIndex(device => device.id === action.payload.id);
                state.devices.splice(index, 1);
                state.highlighted = {};
            }
        });
        builder.addCase(pingTimeDevice.fulfilled, (state, action) => {
            state.pingTime = action.payload;
        });
    }
});

export const { setAdding, setName, setIp, selectDevice, setPort, clearHighlighted, setPinger, setActive, setRefreshFlag } = mainSlice.actions;
export { getDevices, pingDevice, addDevice, pingTimeDevice, checkPortDevice, removeDevice };
export default mainSlice.reducer;