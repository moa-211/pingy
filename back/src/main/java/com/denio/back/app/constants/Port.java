package com.denio.back.app.constants;

public enum Port {
    BAD_IP("Этот хост неизвестен"),
    UNREACHABLE("Порт недоступен"),
    BAD_PORT("Некорректный порт"),
    SUCCESS("Порт доступен");

    private final String message;

    Port(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
