package com.denio.back.app.interfaces;

public interface IAppFactory {
    public IApp createInstance();
}
