package com.denio.back.app.constants;

public enum Login {
    BAD_PASSWORD("Неправильный логин/пароль!");
    private final String message;

    Login(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
