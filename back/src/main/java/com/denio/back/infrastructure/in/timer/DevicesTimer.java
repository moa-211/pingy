package com.denio.back.infrastructure.in.timer;

import com.denio.back.app.interfaces.IApp;
import com.denio.back.infrastructure.builder.interfaces.Built;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;

import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.LocalBean;
import jakarta.enterprise.concurrent.ManagedScheduledExecutorService;
import jakarta.inject.Inject;
import java.util.concurrent.TimeUnit;

@Singleton
@Startup
@LocalBean
public class DevicesTimer {
    @Resource
    ManagedScheduledExecutorService scheduler;

    @Inject
    @Built
    IApp app;

    @PostConstruct
    public void init() {
        this.scheduler.scheduleAtFixedRate(this::run, 500, 5000, TimeUnit.MILLISECONDS);
    }

    public void run() {
        app.pingDevicesSendToClient();
    }
}
