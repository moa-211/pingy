package com.denio.back.app;

import com.denio.back.app.constants.*;
import com.denio.back.app.dto.DeviceDTO;
import com.denio.back.app.interfaces.*;

import com.denio.back.core.factories.DeviceMonitorFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App implements IApp, StorageInjection, TokenManagerInjection, WSControllerInjection {
    public IStorage storage;
    public ITokenManager tokenManager;
    public IClientTransporter clientTransporter;

    // AuthController
    @Override
    public String login(String username, String password) {
        String token = tokenManager.generateToken(username, password);
        int id = storage.findUser(username, password);
        String result = token + " " + id;
        return id != -1 ? result : Login.BAD_PASSWORD.getMessage();
    }

    @Override
    public Register register(String username, String password) {
        if (password.length() < 8) return Register.SHORT_PASSWORD;
        if (storage.addUser(username, password)) return Register.SUCCESS;
        return Register.USER_EXISTS;
    }


    // PingController
    @Override
    public Ping ping(String ipAddress) {
       int result = new DeviceMonitorFactory().createInstance().ping(ipAddress);
       return switch (result) {
           case -1 -> Ping.UNREACHABLE;
           case -2 -> Ping.BAD_IP;
           case -3 -> Ping.UNKNOWN;
           default -> Ping.SUCCESS;
       };
    }

    @Override
    public long ping(String ipAddress, boolean timeSpent) {
        return new DeviceMonitorFactory().createInstance().ping(ipAddress, timeSpent);
    }

    @Override
    public void pingDevicesSendToClient() {
        for (String username : clientTransporter.getActiveClientNames()) {
            List<DeviceDTO> devices = storage.getDevices(username);
            if (devices.isEmpty()) return;

            StringBuilder message = new StringBuilder();
            for (DeviceDTO device : devices) {
                Ping result = this.ping(device.getIp());
                message.append(device.getId()).append("/").append(result.getMessage()).append(",");
            }
            message.deleteCharAt(message.length() - 1);
            clientTransporter.sendToUser(username, message.toString());
        }
    }

    @Override
    public Port checkPort(String ipAddress, int portNumber) {
        int result = new DeviceMonitorFactory().createInstance().checkPort(ipAddress, portNumber);
        return switch (result) {
            case -1 -> Port.UNREACHABLE;
            case -2 -> Port.BAD_IP;
            case -3 -> Port.BAD_PORT;
            default -> Port.SUCCESS;
        };
    }

    // Device logic
    @Override
    public List<DeviceDTO> getDevices(String username) {
        return storage.getDevices(username);
    }

    @Override
    public int addDevice(String ipAddress, String name, String username) {
        int id = storage.addDevice(ipAddress, name, username);
        clientTransporter.sendToUser(username, "REFRESH");
        return id;
    }

    @Override
    public Device removeDevice(int id) {
        String username = storage.getDeviceOwner(id);
        if (storage.removeDevice(id)) {
            clientTransporter.sendToUser(username, "REFRESH");
            return Device.REMOVE_SUCCESS;
        }
        else return Device.NOT_EXISTS;
    }


    // Token logic
    @Override
    public Map<String, String> validateToken(String token) {
        Map<String, String> userInfo = tokenManager.getTokenInfo(token);
        if (storage.findUser(userInfo.get("username"), userInfo.get("password")) != -1) return userInfo;
        else return new HashMap<>();
    }


    // Inject
    @Override
    public void injectStorage(IStorage storage) {
        this.storage = storage;
    }

    @Override
    public void injectTokenManager(ITokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @Override
    public void injectWSController(IClientTransporter wsController) {
        this.clientTransporter = wsController;
    }
}
