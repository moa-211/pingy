export default class Message {
    #message

    constructor(message) {
        this.#message = message;
    }

    get message() {
        return this.#message;
    }

    set message(message) {
        this.#message = message;
    }
};