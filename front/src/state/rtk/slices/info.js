import { createSlice } from '@reduxjs/toolkit';
import { login, register } from './auth.js';
import { addDevice, checkPortDevice, removeDevice } from './main.js';

const infoSlice = createSlice({
    name: 'info',
    initialState: {
        message: ''
    },
    reducers: {
        setMessage: (state, action) => {
            state.message = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(register.fulfilled, (state, action) => {
            state.message = action.payload;
        });
        builder.addCase(login.fulfilled, (state, action) => {
            if (!action.payload.includes('.')) state.message = action.payload;
        });
        builder.addCase(addDevice.fulfilled, (state, action) => {
            if (action.payload.id !== -1)
                state.message = 'Устройство успешно добавлено';
            else state.message = 'При добавлении устройства возникла ошибка';
        });
        builder.addCase(checkPortDevice.fulfilled, (state, action) => {
            state.message = action.payload;
        });
        builder.addCase(removeDevice.fulfilled, (state, action) => {
            state.message = action.payload.result;
        });
    }
});

export const { setMessage } = infoSlice.actions;
export default infoSlice.reducer;