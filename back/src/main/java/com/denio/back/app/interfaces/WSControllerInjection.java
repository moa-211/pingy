package com.denio.back.app.interfaces;

public interface WSControllerInjection {
    public void injectWSController(IClientTransporter wsController);
}
