package com.denio.back.infrastructure.out.data.interfaces;

import com.denio.back.app.interfaces.IStorage;

public interface IStorageFactory {
    public IStorage createInstance();
}
