import LoginForm from '../../component/login-form/component';
import ErrorWindow from '../../component/message-window/component';
import styles from './style.module.css';

function LoginPage() {
    return (
        <div className={ styles.loginPage }>
            <LoginForm />
            <ErrorWindow />
        </div>
    );
}

export default LoginPage;