async function register(username, password) {
    let response = await fetch("http://localhost:8080/back-1.0/api/v1/auth", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    });

    return response;
}

async function login(username, password) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/auth?username=${username}&password=${password}`);
    
    return response;
}

async function getDevices(token) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/device`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return response;
}

async function addDevice(device, token) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/device`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(device)
    });

    return response;
}

async function removeDevice(id, token) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/device/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return response;
}

async function ping(ip, token) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/device/monitoring?ip=${ip}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return response;
}

async function pingTime(ip, time, token) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/device/monitoring?ip=${ip}&time=${time}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return response;
}

async function checkPort(ip, port, token) {
    let response = await fetch(`http://localhost:8080/back-1.0/api/v1/device/monitoring?ip=${ip}&port=${port}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return response;
}

export { register, login, getDevices, addDevice, ping, removeDevice, pingTime, checkPort }; 