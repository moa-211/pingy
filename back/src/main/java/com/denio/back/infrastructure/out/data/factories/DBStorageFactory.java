package com.denio.back.infrastructure.out.data.factories;

import com.denio.back.infrastructure.builder.interfaces.Product;
import com.denio.back.infrastructure.out.data.DBStorage;
import com.denio.back.app.interfaces.IStorage;
import com.denio.back.infrastructure.out.data.interfaces.IStorageFactory;
import jakarta.enterprise.inject.Produces;

public class DBStorageFactory implements IStorageFactory {
    @Override
    @Produces
    @Product
    public IStorage createInstance() {
        return new DBStorage();
    }
}
