package com.denio.back.infrastructure.builder;

import com.denio.back.app.interfaces.*;
import com.denio.back.infrastructure.builder.interfaces.Built;
import com.denio.back.infrastructure.builder.interfaces.IBuilder;
import com.denio.back.infrastructure.builder.interfaces.Product;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;

public class Builder implements IBuilder {
    @Inject
    @Default
    private IApp app;

    @Inject
    @Product
    private IStorage storage;

    @Inject
    @Default
    private ITokenManager tokenManager;

    @Inject
    @Default
    private IClientTransporter wsController;

    @Produces
    @Built
    public IApp buildApp() {
        ((StorageInjection) app).injectStorage(storage);
        ((TokenManagerInjection) app).injectTokenManager(tokenManager);
        ((WSControllerInjection) app).injectWSController(wsController);
        return app;
    }
}
