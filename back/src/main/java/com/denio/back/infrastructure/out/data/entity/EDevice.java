package com.denio.back.infrastructure.out.data.entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "\"DEVICES\"")
public class EDevice implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;

    @Column(name = "ip")
    private String ip;

    @Column(name = "name")
    private String name;

    @Column(name = "user_id")
    private Integer user_id;

//    @ManyToOne
//    @JoinColumn(name = "id", referencedColumnName = "user_id")
//    private EUser user;

    public Integer getID() {
        return ID;
    }

    public String getIp() {
        return ip;
    }

    public String getName() {
        return name;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}

