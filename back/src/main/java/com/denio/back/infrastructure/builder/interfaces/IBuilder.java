package com.denio.back.infrastructure.builder.interfaces;

import com.denio.back.app.interfaces.IApp;

public interface IBuilder {
    public IApp buildApp();
}


