import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import QuerierInjection from './injection';


const querier = new QuerierInjection().querier;


const register = createAsyncThunk('auth/register', async (arg, { getState }) => {
    const state = getState();
    const result = await querier.register(state.auth.username, state.auth.password);
    return result.message;
});

const login = createAsyncThunk('auth/login', async (arg, { getState }) => {
    const state = getState();
    const result = await querier.login(state.auth.username, state.auth.password);
    return result.message;
});

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        username: '',
        password: '',
        token: '',
        id: ''
    },
    reducers: {
        setUsername: (state, action) => {
            state.username = action.payload;
        },
        setPassword: (state, action) => {
            state.password = action.payload;
        },
        setToken: (state, action) => {
            state.token = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(login.fulfilled, (state, action) => {
            if (action.payload.includes('.')) {
                const info = action.payload.split(' ');
                state.token = info[0];
                state.id = info[1];
            }
        });
    }
});

export const { setUsername, setPassword, setToken, setMessage } = authSlice.actions;
export { register, login };
export default authSlice.reducer;