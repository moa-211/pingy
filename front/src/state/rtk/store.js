import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { persistStore, 
         persistReducer,
         FLUSH,
         REHYDRATE,
         PAUSE,
         PERSIST,
         PURGE,
         REGISTER } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import authReducer from './slices/auth.js';
import mainReducer from './slices/main.js';
import infoReducer from './slices/info.js';

const persistConfig = {
    key: 'root',
    storage
};

export const rootReducer = combineReducers({
    auth: authReducer,
    main: mainReducer,
    info: infoReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
            }
        })
});

export const persistor = persistStore(store);
export default store;