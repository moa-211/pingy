import { useEffect } from 'react';
import { useAddingListener, 
         useDeviceDispatcher, 
         useHighlightedListener, 
         useMessageDispatcher, 
         usePingTimeListener, 
         usePortDispatcher, 
         usePortListener, 
         useHighlightedDispatcher } from '../../../state/rtk/api';
import MessageWindow from '../message-window/component';
import styles from './style.module.css';

function DeviceInfo() {
    const adding = useAddingListener();
    const device = useHighlightedListener();
    const highlightDispatcher = useHighlightedDispatcher();
    const dispatcher = useDeviceDispatcher();
    const setPort = usePortDispatcher();
    const pingTime = usePingTimeListener();
    const setMessage = useMessageDispatcher();
    const port = usePortListener();

    const handlePortChange = (event) => {
        setPort(Number(event.target.value));
    };

    const handlePingTime = (event) => {
        event.preventDefault();
        dispatcher.pingTime(device.id);
    };

    const handlePortCheck = (event) => {
        event.preventDefault();
        if (port === 0 || port === '') {
            setMessage('Укажите порт!');
            return;
        }
        dispatcher.checkPort();
    };
    
    const handleRemove = (event) => {
        event.preventDefault();
        dispatcher.remove(device.id);
    };
    
    useEffect(() => {
        return () => {
            setPort('');
            setMessage('');
            highlightDispatcher.clear();
        };
    }, []);

    return (
        <form className={ styles.deviceInfo }>
            <div className={ styles.info }>
                <span>Название устройства: {device.name}</span>
                <span>Адрес устройства: {device.ip}</span>
            </div>
            <div className={ styles.port }>
                <input value={port !== 0 ? port : ''} type="number" min="1" max="65535" placeholder="Введите порт" onChange={handlePortChange} disabled={!device.active}/>
                <button disabled={!device.active} onClick={handlePortCheck}>Проверить порт</button>
            </div>
            <div className={ styles.pingTime }>
                <span>Задержка: {pingTime === 0 ? '' : pingTime}</span>
                <button onClick={handlePingTime} disabled={!device.active}>Вычислить задержку</button>
            </div>
            <button onClick={handleRemove} disabled={!device.id}>Удалить устройство</button>
            {!adding && (<MessageWindow />)}
        </form>
    )
}

export default DeviceInfo;