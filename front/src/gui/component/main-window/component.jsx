import { useDevicesDispatcher, useDevicesListener, useRefreshFlagDispatcher, useRefreshFlagListener, useUsernameListener } from '../../../state/rtk/api';
import Device from '../device/component';
import { useEffect } from 'react';
import styles from './style.module.css';

function MainWindow() {
    const devices = useDevicesListener();
    const dispatcher = useDevicesDispatcher();
    const username = useUsernameListener();
    const refreshFlag = useRefreshFlagListener();
    const resetRefreshFlag = useRefreshFlagDispatcher();

    useEffect(() => {
        dispatcher.get();
        dispatcher.ping(username);
    }, []);

    useEffect(() => {
        if (refreshFlag) {
            dispatcher.get();
            resetRefreshFlag();
        }
    }, [refreshFlag]);

    const devicesToRender = devices.map(device => (<Device key={device.id} id={device.id} />));
    return (
        <div className={ styles.main }>
            {devicesToRender}
        </div>
    )
}

export default MainWindow;