package com.denio.back.infrastructure.in.controller.interfaces;

import com.denio.back.app.dto.ResponseDTO;
import com.denio.back.app.interfaces.IApp;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

import java.util.HashMap;
import java.util.Map;

public interface IController {
    Jsonb jsonb = JsonbBuilder.create();
    ResponseDTO response = new ResponseDTO();
    default Map<String, String> checkToken(HttpHeaders headers, IApp app) {
        Map<String, String> result = new HashMap<>();

        String header = headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (header == null || header.equals("")) {
            result.put("result", "BAD");
            result.put("message", "Не указан заголовок авторизации");
            return result;
        }

        String token = headers.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (!token.contains("Bearer ")) {
            result.put("result", "BAD");
            result.put("message", "Некорректный заголовок авторизации");
            return result;
        }

        token = token.replace("Bearer ", "");

        if (token.matches("|undefined|null")) {
            result.put("result", "BAD");
            result.put("message", "Не указан токен");
            return result;
        }

        Map<String, String> userInfo = app.validateToken(token);
        if (userInfo.isEmpty()) {
            result.put("result", "BAD");
            result.put("message", "Некорректный токен");
            return result;
        }

        result.put("result", "GOOD");
        result.putAll(userInfo);
        return result;
    }

    default Response sendResponse(String message) {
        response.setMessage(message);
        String resultJSON = jsonb.toJson(response);

        return Response.ok(resultJSON).build();
    }

    default Response sendResponse(String message, Response.Status status) {
        response.setMessage(message);
        String resultJSON = jsonb.toJson(response);

        return Response.status(status).entity(resultJSON).build();
    }
}
