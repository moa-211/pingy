package com.denio.back.infrastructure.in.controller;

import com.denio.back.app.constants.Device;
import com.denio.back.app.dto.DeviceDTO;
import com.denio.back.app.interfaces.IApp;
import com.denio.back.infrastructure.builder.interfaces.Built;
import com.denio.back.infrastructure.in.controller.interfaces.IController;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.Map;

@Path("/device")
public class DeviceController implements IController {
    Jsonb jsonb = JsonbBuilder.create();
    @Inject
    @Built
    private IApp app;

    @Context
    private HttpHeaders headers;

    @GET
    @Produces("application/json")
    public Response getDevices() {
        Map<String, String> tokenCheck = checkToken(headers, app);
        if (tokenCheck.get("result").equals("BAD"))
            return sendResponse(tokenCheck.get("message"), Response.Status.UNAUTHORIZED);

        List<DeviceDTO> result = app.getDevices(tokenCheck.get("username"));
        return sendResponse(jsonb.toJson(result));
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response addDevice(String dto) {
        Map<String, String> tokenCheck = checkToken(headers, app);
        if (tokenCheck.get("result").equals("BAD"))
            return sendResponse(tokenCheck.get("message"), Response.Status.UNAUTHORIZED);

        DeviceDTO body;

        try {
            body = jsonb.fromJson(dto, DeviceDTO.class);
        }
        catch (Exception e) {
            return sendResponse("Некорректный запрос", Response.Status.BAD_REQUEST);
        }

        String ip = body.getIp();
        String name = body.getName();
        String username = tokenCheck.get("username");

        if (ip == null || name == null)
            return sendResponse("Некорректный запрос", Response.Status.BAD_REQUEST);

        int result = app.addDevice(ip, name, username);
        return sendResponse(String.valueOf(result));
    }

    @DELETE
    @Produces("application/json")
    @Path("/{id}")
    public Response removeDevice(
            @PathParam("id")
            @DefaultValue("")
            String ID) {
        Map<String, String> tokenCheck = checkToken(headers, app);
        if (tokenCheck.get("result").equals("BAD"))
            return sendResponse(tokenCheck.get("message"), Response.Status.UNAUTHORIZED);

        if (ID.matches("|null|undefined"))
            return sendResponse("Не указан ID устройства", Response.Status.BAD_REQUEST);

        Device result = app.removeDevice(Integer.parseInt(ID));
        return sendResponse(result.getMessage());
    }
}
