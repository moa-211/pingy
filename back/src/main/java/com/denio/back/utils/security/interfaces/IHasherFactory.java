package com.denio.back.utils.security.interfaces;

public interface IHasherFactory {
    public IHasher createInstance();
}
