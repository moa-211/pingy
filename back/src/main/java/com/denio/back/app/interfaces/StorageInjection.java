package com.denio.back.app.interfaces;

public interface StorageInjection {
    public void injectStorage(IStorage explorer);
}
