package com.denio.back.app.constants;

public enum Ping {
    SUCCESS("Хост доступен"),
    UNREACHABLE("Хост не доступен"),
    BAD_IP("Этот хост неизвестен"),
    UNKNOWN("Неизвестная ошибка");
    private final String message;

    Ping(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
