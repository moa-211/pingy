package com.denio.back.app.factories;

import com.denio.back.app.App;
import com.denio.back.app.interfaces.IApp;
import com.denio.back.app.interfaces.IAppFactory;


public class AppFactory implements IAppFactory {
    @Override
    public IApp createInstance() {
        return new App();
    }
}
