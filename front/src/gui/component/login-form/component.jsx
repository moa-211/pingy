import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useUsernameListener, useUsernameDispatcher,
         usePasswordListener, usePasswordDispatcher,
         useTokenListener,
         useMessageDispatcher,
         useRegisterDispatcher,
         useLoginDispatcher } from '../../../state/rtk/api';

import styles from './style.module.css';

function LoginForm() {
    const username = useUsernameListener();
    const setUsername = useUsernameDispatcher();
    const password = usePasswordListener();
    const setPassword = usePasswordDispatcher();
    const token = useTokenListener();
    const setMessage = useMessageDispatcher();

    const register = useRegisterDispatcher();
    const login = useLoginDispatcher();
    
    const navigate = useNavigate();

    const handleUsername = event => setUsername(event.target.value);
    const handlePassword = event => setPassword(event.target.value);

    const handleAuth = event => {
        if (!username) {
            setMessage('Введите логин!');
            return;
        }
        if (!password) {
            setMessage('Введите пароль!');
            return;
        }

        if (event.target.id === 'register') register();
        else login();
    };

    useEffect(() => {
        if (token !== '') navigate('/main');
    }, [token]);

    return (
        <>
            <div className={ styles.loginForm }>
                <input type="text" placeholder="Имя пользователя" value={username} onChange={handleUsername} />
                <input type="password" placeholder="Пароль" value={password} onChange={handlePassword} />
            </div>
            <div className={ styles.loginFormBtns }>
                <button onClick={handleAuth}>Войти</button>
                <button id="register" onClick={handleAuth}>Зарегистрироваться</button>
            </div>
        </>
    );
}

export default LoginForm;