export default class Device {
    #id
    #ip
    #name
    
    constructor(id, ip, name) {
        this.#id = id;
        this.#ip = ip;
        this.#name = name;
    }

    get id() {
        return this.#id;
    }

    set id(id) {
        this.#id = id;
    }

    get ip() {
        return this.#ip;
    }

    set ip(ip) {
        this.#ip = ip;
    }

    get name() {
        return this.#name;
    }

    set name(name) {
        this.#name = name;
    }
};