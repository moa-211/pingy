import AddWindow from "../../component/add-window/component";
import MainWindow from "../../component/main-window/component";
import DeviceInfo from "../../component/device-info/component";
import { useAddingDispatcher } from "../../../state/rtk/api";
import LogoutWindow from "../../component/logout-window/component";

function MainPage() {
    const setAdding = useAddingDispatcher();

    const handleClick = () => setAdding(true);
    
    return (
        <>
            <button onClick={handleClick}>+</button>
            <MainWindow />
            <AddWindow />
            <DeviceInfo />
            <LogoutWindow />
        </>
    );
}

export default MainPage;