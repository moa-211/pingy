import { setPingResult, setRefresh } from '../../state/redux/wsMethods.js';

export default class WSTransporter {
    static instance = null;
    connection;
    
    constructor(id) {
        if (WSTransporter.instance) return WSTransporter.instance;

        this.connection = new WebSocket('ws://localhost:8080/back-1.0/monitoring');
        this.connection.onmessage = (event) => {
            const message = event.data;
            if (!message.includes('REFRESH')) setPingResult(event.data);
            else setRefresh();
        }
        this.connection.onopen = () => this.setPinger(id);
        WSTransporter.instance = this;
        return this;
    }

    setPinger(id) {
        this.connection.send(id);
    }
};