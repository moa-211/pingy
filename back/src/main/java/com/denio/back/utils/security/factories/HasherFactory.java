package com.denio.back.utils.security.factories;

import com.denio.back.utils.security.Hasher;
import com.denio.back.utils.security.interfaces.IHasher;
import com.denio.back.utils.security.interfaces.IHasherFactory;

public class HasherFactory implements IHasherFactory {
    @Override
    public IHasher createInstance() {
        return new Hasher();
    }
}
