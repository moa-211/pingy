import Device from './dto/device.js';
import Message from './dto/message.js';
import { register, login, getDevices, addDevice, removeDevice, ping, pingTime, checkPort } from './rest.js';

export default class Querier {
    async register(username, password) {
        const response = await register(username, password);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);
    }

    async login(username, password) {
        const response = await login(username, password);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);
    }

    async getDevices(token) {
        const response = await getDevices(token);
        const content = await this.#transform(response);
        return await this.#parseDevices(content);
    }

    async addDevice(device, token) {
        const response = await addDevice(device, token);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);
    }

    async removeDevice(id, token) {
        const response = await removeDevice(id, token);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);
    }
    
    async ping(ip, token) {
        const response = await ping(ip, token);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);
    }

    async pingTime(ip, token) {
        const response = await pingTime(ip, true, token);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);
    }

    async checkPort(ip, port, token) {
        const response = await checkPort(ip, port, token);
        const content = await this.#transform(response);
        return await this.#parseMessage(content);    
    }

    async #transform(response) {
        const content = await response.json();
        return content.message;
    }

    async #parseMessage(content) {
        return new Message(content);
    }

    async #parseDevices(content) {
        const devices = await JSON.parse(content);
        const result = devices.map(device => new Device(device.id, device.ip, device.name));

        return result;
    }
};