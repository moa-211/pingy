package com.denio.back.utils.uuid.interfaces;

public interface IUUIDGeneratorFactory {
    public IUUIDGenerator createInstance();
}
