$domainName = "domain1"
$targetPath = "C:\pingy\back\target" 
$domainsPath = "C:\Jakarta\glassfish\glassfish\domains"
$warName = "back-1.0.war"

asadmin stop-domain $domainName
Start-Sleep -Seconds 1
Remove-Item -Path $targetPath -Recurse -Force
Remove-Item -Path "$domainsPath\$domainName\applications" -Recurse -Force
Remove-Item -Path "$domainsPath\$domainName\osgi-cache" -Recurse -Force
Remove-Item -Path "$domainsPath\$domainName\generated" -Recurse -Force
mvn compile
mvn package
asadmin start-domain $domainName
asadmin deploy --force=true "$targetPath\$warName"
