package com.denio.back.utils.uuid;

import com.denio.back.utils.uuid.interfaces.IUUIDGenerator;

import java.util.UUID;

public class UUIDGenerator implements IUUIDGenerator {
    public String getUUID() {
        return UUID.randomUUID().toString();
    }
}
