package com.denio.back.utils.security.interfaces;

public interface IHasher {
    public String encode(String text);
}
