import { useEffect } from 'react';
import { useMessageDispatcher, useMessageListener } from '../../../state/rtk/api';
import styles from './style.module.css';

function MessageWindow() {
    const message = useMessageListener();
    const setMessage = useMessageDispatcher();

    useEffect(() => {
        if (message !== '') setTimeout(() => setMessage(''), 2000);
    }, [message]);

    if (message !== '') return (
        <div className={ styles.messageWindow }>
            <span>{message}</span>
        </div>
    )
    else return null;
}

export default MessageWindow;