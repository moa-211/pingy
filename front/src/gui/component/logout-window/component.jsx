import { useEffect } from "react";
import { usePasswordDispatcher, useTokenDispatcher, useUsernameDispatcher, useUsernameListener, useTokenListener } from "../../../state/rtk/api";
import { useNavigate } from 'react-router-dom';

function LogoutWindow() {
    const username = useUsernameListener();
    const token = useTokenListener();
    const setUsername = useUsernameDispatcher();
    const setPassword = usePasswordDispatcher();
    const setToken = useTokenDispatcher();
    const navigate = useNavigate();

    const handleLogout = event => {
        event.preventDefault();
        setUsername('');
        setPassword('');
        setToken('');
    };

    useEffect(() => {
        if (token === '') navigate('/');
    }, [token]);

    return (
        <div>
            <span>Имя пользователя: {username}</span>
            <button onClick={handleLogout}>Выйти</button>
        </div>
    );
}

export default LogoutWindow;