package com.denio.back.app.constants;

public enum Register {
    USER_EXISTS ("Пользователь с таким именем уже существует"),
    SHORT_PASSWORD ("Пароль должен состоять из не менее 8 символов"),
    SUCCESS("Регистрация прошла успешно");

    private final String message;

    Register(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
