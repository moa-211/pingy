import { setActive, setRefreshFlag } from "../rtk/slices/main";
import store from "../rtk/store";

export function setPingResult(text) {
    let messages = text.split(',');
    messages.forEach(message => {
        let [id, status] = message.split('/');
        store.dispatch(setActive({id: Number(id), status: status === 'Хост доступен'}));
    });
}

export function setRefresh() {
    store.dispatch(setRefreshFlag(true));
}