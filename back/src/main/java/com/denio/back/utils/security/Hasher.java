package com.denio.back.utils.security;

import com.denio.back.utils.security.interfaces.IHasher;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.spec.KeySpec;
import java.util.Base64;

public class Hasher implements IHasher {
    byte[] salt = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
                   0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13,
                   0x14, 0x15};
    @Override
    public String encode(String text) {
        String encoded = "";
        try {
            KeySpec spec = new PBEKeySpec(text.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = f.generateSecret(spec).getEncoded();
            Base64.Encoder enc = Base64.getEncoder();
            encoded = enc.encodeToString(hash);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in Hasher");
            return encoded;
        }

        return encoded;
    }
}
