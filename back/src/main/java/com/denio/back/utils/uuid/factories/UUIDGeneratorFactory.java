package com.denio.back.utils.uuid.factories;

import com.denio.back.utils.uuid.UUIDGenerator;
import com.denio.back.utils.uuid.interfaces.IUUIDGenerator;
import com.denio.back.utils.uuid.interfaces.IUUIDGeneratorFactory;

public class UUIDGeneratorFactory implements IUUIDGeneratorFactory {
    public IUUIDGenerator createInstance() {
        return new UUIDGenerator();
    }
}
