export default class QuerierInjection {
    static instance;
    #querier;

    constructor(impl) {
        if (QuerierInjection.instance) return QuerierInjection.instance;

        QuerierInjection.instance = this;
        this.#querier = impl;
        return this;
    }

    get querier() {
        return this.#querier;
    }

    set querier(impl) {
        this.#querier = impl;
    }
}