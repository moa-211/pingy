package com.denio.back.core.factories;

import com.denio.back.core.DeviceMonitor;
import com.denio.back.core.interfaces.IDeviceMonitor;

public class DeviceMonitorFactory {
    public IDeviceMonitor createInstance() {
        return new DeviceMonitor();
    }
}
