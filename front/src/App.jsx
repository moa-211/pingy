import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import LoginPage from './gui/page/login-page/component';
import MainPage from './gui/page/main-page/component';
import { buildProvider } from './state/rtk/api';

const Provider = buildProvider();

const pgLogin = (<LoginPage />);
const pgMain = (<MainPage />);

function App() {
  return (
    <Provider>
      <Router>
        <Routes>
          <Route path='/' element={pgLogin} />
          <Route path='/main' element={pgMain} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
