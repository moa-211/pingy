package com.denio.back.utils.uuid.interfaces;

public interface IUUIDGenerator {
    public String getUUID();
}
