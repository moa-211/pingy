import QuerierInjection from './state/rtk/slices/injection';
import Querier from './transport/out/querier';

function initQuerier() {
    new QuerierInjection().querier = new Querier();
}

initQuerier();