package com.denio.back.app.interfaces;

import com.denio.back.app.dto.DeviceDTO;

import java.util.List;

public interface IStorage {
    int findUser(String username, String password);
    boolean addUser(String username, String password);
    public String getDeviceOwner(int deviceID);
    List<DeviceDTO> getDevices(String username);
    int addDevice(String ip, String name, String username);
    boolean removeDevice(int id);
}
