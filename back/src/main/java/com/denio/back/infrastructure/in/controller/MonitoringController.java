package com.denio.back.infrastructure.in.controller;

import com.denio.back.app.constants.Ping;
import com.denio.back.app.interfaces.IApp;
import com.denio.back.infrastructure.builder.interfaces.Built;
import com.denio.back.infrastructure.in.controller.interfaces.IController;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

import java.util.Map;

@Path("/device/monitoring")
public class MonitoringController implements IController {
    @Inject
    @Built
    private IApp app;

    @Context
    private HttpHeaders headers;

    @GET
    @Produces("application/json")
    public Response ping(
            @DefaultValue("")
            @QueryParam("ip")
            String ip,
            @DefaultValue("")
            @QueryParam("time")
            String time,
            @DefaultValue("")
            @QueryParam("port")
            String port) {
        Map<String, String> tokenCheck = checkToken(headers, app);
        if (tokenCheck.get("result").equals("BAD"))
            return sendResponse(tokenCheck.get("message"), Response.Status.UNAUTHORIZED);

        if (ip.matches("|undefined|null"))
            return sendResponse("Не указан IP-адрес", Response.Status.BAD_REQUEST);

        if (!ip.isEmpty() && !port.matches("|undefined|null"))
            return sendResponse(app.checkPort(ip, Integer.parseInt(port)).getMessage());

        else if (!ip.isEmpty() && !time.matches("|undefined|null")) {
            long result = app.ping(ip, Boolean.parseBoolean(time));
            String message;
            if (result == -1) message = Ping.UNREACHABLE.getMessage();
            if (result == 0) message = Ping.SUCCESS.getMessage();
            if (result == -2) message = Ping.BAD_IP.getMessage();
            if (result == -3) message = Ping.UNKNOWN.getMessage();
            else message = String.valueOf(result);
            return sendResponse(message);
        }

        return sendResponse(app.ping(ip).getMessage());
    }
}
