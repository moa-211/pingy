package com.denio.back.infrastructure.out.token.factories;

import com.denio.back.infrastructure.out.token.TokenManager;
import com.denio.back.infrastructure.out.token.interfaces.ITokenManagerFactory;
import com.denio.back.app.interfaces.ITokenManager;

public class TokenManagerFactory implements ITokenManagerFactory {
    @Override
    public ITokenManager createInstance() {
        return new TokenManager();
    }
}
