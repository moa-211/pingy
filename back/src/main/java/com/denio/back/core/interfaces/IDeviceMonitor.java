package com.denio.back.core.interfaces;

public interface IDeviceMonitor {
    public int ping(String ipAddress);
    public long ping(String ipAddress, boolean timeSpent);
    public int checkPort(String ipAddress, int portNumber);
}
